# Replacer
Replacer is a Drupal 8 module designed to provide a filtering mechanism by which content can be filtered on the fly. You define "filters" which consist of regex search patterns and replacement values, and select the entities and content types against which you wish the filter to apply.

Replacer does not permanently alter the content! All content continues to be stored in its original form. It simply provides a filter layer in between that content and the browser.

This is a very niche module. Many folks may not have an immediate need for it. But there are/could be instances which arise in which Replacer could save you a ton of work. See the sample
filters section below for some ideas.

NOTE: If you are reading this on Github, we recommend that you visit our <a href="https://www.drupal.org/project/replacer">official project page</a> on Drupal.org and install the module as you would any other Drupal module. Code that is committed at GitHub should be considered in development/bleed edge/etc. 
 
# Requirements

* Drupal 8.x
* PHP 5.5 or higher (untested under PHP 7)
* Entity module installed

# Known Issues

* None at this time

# Installation Instructions

1. Download and install the module
2. Configure the module (admin/config/replacer)
3. Set up your filters (admin/config/replacer, the "Replacer Filters" tab)

# Sample Filters

Your company name has changed. What do you do with a large site with a bazillion references to your old company name? A simple filter for a large site might search for all instances of "Acme, Inc." and replace with "Superstar Productions, Inc.", saving you from editing hundreds or even thousands of assets:

http://incurs.us/sites/default/files/inline-images/replacer-1.png

Consider the scenario where your PR Manager/Media Contact gets married, and has a last name change. Her maiden name was "Hughes", and now her last name is "Hughes-Smith". Your site could potentially have hundreds of references to her original last name. Using a simple search filter, or even backreferences, as this example shows, could easily solve this:

http://incurs.us/sites/default/files/inline-images/replacer-2.png

Finally, consider the scenario where your PR Manager/Media Contact has changed completely. Your site has dozens or even hundreds of press releases which now contain old contact data. You
could set up a filter similar to this to alter the individual's name on the fly. However, since all content continues to be stored in its original form, you will always have a record of
who the media contact was when the content was first published.

http://incurs.us/sites/default/files/inline-images/replacer-3.png

