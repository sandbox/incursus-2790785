<?php

namespace Drupal\replacer\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Builds the list of associations for the channel path map form.
 */
class ReplacerFilterListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    module_load_include('inc', 'replacer');
    $header['filterId'] = $this->t('Filter ID');
    $header['searchPattern'] = $this->t('Search Pattern');
    $header['filterValue'] = $this->t('Filter Value');
    $header['entityTypes'] = $this->t('Entity Types');
    $header['contentTypes'] = $this->t('Content Types');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['filterId'] = $entity->id();

    $row['searchPattern'] = $entity->getSearchPattern();
    $row['filterValue'] = $entity->getFilterValue();
    $row['filterValue'] = $entity->getFilterValue();

    $savedEntityTypes = $entity->getEntityTypes();
    $entityTypes = array();

    foreach ($savedEntityTypes as $index => $eType) {
      if ($eType !== 0) {
        $entityTypes[] = $eType;
      }
    }

    $row['entityTypes'] = implode(', ', $entityTypes);

    $savedContentTypes = $entity->getContentTypes();
    $contentTypes = array();

    foreach ($savedContentTypes as $index => $cType) {
      if ($cType !== 0) {
        $contentTypes[] = $cType;
      }
    }

    $row['contentTypes'] = implode(', ', $contentTypes);

    return $row + parent::buildRow($entity);
  }

}
