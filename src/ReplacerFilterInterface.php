<?php

namespace Drupal\replacer;

/**
 * @file
 * Contains \Drupal\captcha\ChannelPathMapInterface.
 */

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ChannelPathMapInterface.
 *
 * @package Drupal\replacer
 *
 * Provides an interface defining a CaptchaPoint entity.
 */
interface ReplacerFilterInterface extends ConfigEntityInterface {

  /**
   * Getter for id property.
   *
   * @return string
   *   Label string.
   */
  public function id();

  /**
   * Setter for filterId property.
   *
   * @param string $filterId
   *   Map entry machine ID string.
   */
  public function setFilterId($filterId);

  /**
   * Getter for filterId property.
   *
   * @return string
   *   Label string.
   */
  public function getFilterId();

  /**
   * Setter for searchPattern property.
   *
   * @param string $searchPattern
   *   Label string.
   */
  public function setSearchPattern($searchPattern);

  /**
   * Getter for filterValue property.
   *
   * @return string
   *   Label string.
   */
  public function getFilterValue();

  /**
   * Setter for filterValue property.
   *
   * @param string $filterValue
   *   Label string.
   */
  public function setFilterValue($filterValue);

  /**
   * Getter for contentTypes property.
   *
   * @return string
   *   Label string.
   */
  public function getContentTypes();

  /**
   * Setter for contentTypes property.
   *
   * @param array $contentTypes
   *   Label string.
   */
  public function setContentTypes($contentTypes);

  /**
   * Getter for entityTypes property.
   *
   * @return string
   *   Label string.
   */
  public function getEntityTypes();

  /**
   * Setter for entityTypes property.
   *
   * @param array $entityTypes
   *   Label string.
   */
  public function setEntityTypes($entityTypes);

}
