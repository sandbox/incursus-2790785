<?php

namespace Drupal\replacer\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Form to edit CAPTCHA points.
 */
class ReplacerFilterForm extends EntityForm {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a ContentLanguageSettingsForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    module_load_include('inc', 'replacer');

    /* @var CaptchaPointInterface $captchaPoint */
    $filterEntry = $this->entity;

    // Support to set a default map_id through a query argument.
    $request = \Drupal::request();
    if ($filterEntry->isNew() && !$filterEntry->id() && $request->query->has('mapId')) {
      $filterEntry->set('mapId', $request->query->get('mapId'));
      $filterEntry->set('searchPattern', $request->query->get('searchPattern'));
      $filterEntry->set('filterValue', $request->query->get('filterValue'));
      $filterEntry->set('contentTypes', $request->query->get('contentTypes'));
    }

    $form['filterId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter ID/Name'),
      '#default_value' => $filterEntry->getFilterId(),
      '#required' => TRUE,
    ];

    $form['searchPattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Pattern'),
      '#default_value' => $filterEntry->getSearchPattern(),
      '#required' => TRUE,
    ];

    $form['filterValue'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Replacement Value'),
      '#default_value' => $filterEntry->getFilterValue(),
      '#required' => FALSE,
    ];

    // $bundles = $this->entityManager->getAllBundleInfo();
    // foreach ($bundles as $bundleType => $b)
    // $entityTypes[ $bundleType ] = $bundleType;.
    $entityTypes = array(
      'node' => 'Node',
      'block_content' => 'Block Content',
    );

    $form['entityTypes'] = [
      '#type' => 'checkboxes',
      '#options' => $entityTypes,
      '#title' => $this->t('Entity Types'),
      '#default_value' => $filterEntry->getEntityTypes(),
      '#required' => TRUE,
    ];

    // Get content types so we can choose to exclude certain content types from the dashboard.
    $contentTypes = node_type_get_names();

    $form['contentTypes'] = [
      '#type' => 'checkboxes',
      '#options' => $contentTypes,
      '#title' => $this->t('Content Types'),
      '#default_value' => $filterEntry->getContentTypes(),
      '#required' => FALSE,
    ];

    $form['filterId'] = [
      '#type' => 'machine_name',
      '#default_value' => $filterEntry->id(),
      '#machine_name' => [
        'exists' => 'replacer_filter_load',
      ],
      '#disable' => !$filterEntry->isNew(),
      '#required' => TRUE,
    ];

    // Select widget for CAPTCHA type.
    // $form['captchaType'] = [
    // '#type' => 'select',
    // '#title' => t('Challenge type'),
    // '#description' => t('The CAPTCHA type to use for this form.'),
    // '#default_value' => ($filterEntry->getCaptchaType() ?: $this->config('captcha.settings')->get('default_challenge')),
    // '#options' => _captcha_available_challenge_types(),
    // ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /* @var ReplacerFilter $filterEntry */
    $filterEntry = $this->entity;
    // Echo '<pre>'; print_r($filterEntry); echo '</pre>';.
    $status = $filterEntry->save();

    // $e = \Drupal::entityTypeManager()->getStorage('replacer_filter')->loadMultiple();
    // echo '<pre>'; print_r($e); echo '</pre>';
    // exit;.
    if ($status == SAVED_NEW) {
      drupal_set_message($this->t('Replacer Filter for %mapId form was created.', [
        '%mapId' => $filterEntry->getFilterId(),
      ]));
    }
    else {
      drupal_set_message($this->t('Replacer Filter for %mapId form was updated.', [
        '%mapId' => $filterEntry->getFilterId(),
      ]));
    }
    $form_state->setRedirect('replacer_filter.list');
  }

}
