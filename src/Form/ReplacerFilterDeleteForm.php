<?php

namespace Drupal\replacer\Form;

/**
 * @file
 * Contains \Drupal\replacer\Form\ReplacerFilterDeleteForm.
 */

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a ReplacerFilter entry.
 */
class ReplacerFilterDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('replacer_filter.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    drupal_set_message($this->t('Replacer Filter entry %id has been deleted.', ['%id' => $this->entity->id()]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
