<?php

namespace Drupal\replacer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure form.
 */
class ConfigureForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'replacer_configure_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return [
      'replacer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('replacer.settings');

    // Pusher.com Connection Settings.
    $form['replacer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacer Connection Settings'),
    );

    $form['replacer']['debugLogging'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable debug logging to Drupal Watchdog'),
      '#required' => FALSE,
      '#default_value' => $config->get('debugLogging'),
      '#description' => t('It goes without saying that this should not be enabled in production environments! But if you need a quick and dirty debug log to watchdog, enable this.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('replacer.settings');

    // General settings.
    $config->set('debugLogging', $form_state->getValue('debugLogging'))
           ->save();

    parent::submitForm($form, $form_state);
  }

}
