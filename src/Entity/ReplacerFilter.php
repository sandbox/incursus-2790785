<?php

namespace Drupal\replacer\Entity;

/**
 * @file
 * Contains \Drupal\replacer\Entity\ReplacerFilter.
 */

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\replacer\ReplacerFilterInterface;

/**
 * Defines the ReplacerFilter entity.
 *
 * @ConfigEntityType(
 *   id = "replacer_filter",
 *   label = @Translation("Replacer Filter Entry"),
 *   handlers = {
 *     "list_builder" = "Drupal\replacer\Controller\ReplacerFilterListBuilder",
 *     "form" = {
 *       "add" = "Drupal\replacer\Form\ReplacerFilterForm",
 *       "delete" = "Drupal\replacer\Form\ReplacerFilterDeleteForm",
 *       "edit" = "Drupal\replacer\Form\ReplacerFilterForm",
 *     }
 *   },
 *   config_prefix = "replacer_filter",
 *   admin_permission = "administer site configuration",
 *   list_cache_tags = {
 *    "rendered"
 *   },
 *   entity_keys = {
 *     "id" = "filterId",
 *   },
 *   config_export = {
 *     "filterId",
 *     "searchPattern",
 *     "filterValue",
 *     "contentTypes",
 *     "entityTypes"
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/replacer/{id}/delete",
 *     "edit-form" = "/admin/config/replacer/{id}/edit"
 *   }
 * )
 */
class ReplacerFilter extends ConfigEntityBase implements ReplacerFilterInterface {

  public $filterId;
  protected $searchPattern;
  protected $filterValue;
  protected $contentTypes;
  protected $entityTypes;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->filterId;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilterId() {
    return $this->filterId;
  }

  /**
   * {@inheritdoc}
   */
  public function setFilterId($filterId) {
    $this->filterId = $filterId;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchPattern() {
    return $this->searchPattern;
  }

  /**
   * {@inheritdoc}
   */
  public function setSearchPattern($searchPattern) {
    $this->searchPattern = $searchPattern;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilterValue() {
    return $this->filterValue;
  }

  /**
   * {@inheritdoc}
   */
  public function setFilterValue($filterValue) {
    $this->filterValue = $filterValue;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentTypes() {
    return $this->contentTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function setContentTypes($contentTypes) {
    $this->contentTypes = $contentTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypes() {
    return $this->entityTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityTypes($entityTypes) {
    $this->entityTypes = $entityTypes;
  }

}
